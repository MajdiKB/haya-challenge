import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HousesList from "../views/HousesList.vue";
import Home from "../views/Home.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/:catchAll(.*)",
    name: "NotFound",
    component: Home,
    redirect: "/",
  },
  {
    path: "/houses/page=:page",
    name: "Houses",
    component: HousesList,
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
