import axios from "axios";
import ListApiResponseModel from "@/Models/ListApiResponseModel";
export default {
  get(url: string): Promise<ListApiResponseModel> {
    return axios.get(url);
  },
};
