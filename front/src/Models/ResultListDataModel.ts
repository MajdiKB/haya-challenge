export default interface ResultListDataModel {
  bathrooms: number;
  created_at: string;
  id: number;
  meters: number;
  name: string;
  photo: string;
  price: number;
  rooms: number;
  type: string;
  updated_at: string;
  city: string;
}
