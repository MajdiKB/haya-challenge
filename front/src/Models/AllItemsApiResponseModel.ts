import ResultDataModel from "./ResultDataModel";

export default interface AllItemsApiResponseModel {
  config: {};
  data: ResultDataModel;
  headers: {};
  request: XMLHttpRequest;
  status: number;
  statusText: string;
}
