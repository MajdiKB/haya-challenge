# Welcome to HAYA REAL ESTATE challenge!!

*Welcome to my technical test that I have developed for the company "HAYA REAL ESTATE" located in Spain.*

# To use the App, follow the instructions below...

## Clone the repository in a local folder.

`git@gitlab.com:MajdiKB/haya-challenge.git`

## Back, database.

The database was developed with the Framework *Laravel*, then install dependencies.

In the folder:

*back/*

execute:

`npm i`

`composer i`

#### Create your mysql database

In your terminal execute:

`mysql -u root -p`

and create a database:

`create database Haya;`

#### Create your own .env

In the folder:

*back/*

execute:

`cp .env.example .env`

and generate your Laravel Key...

`php artisan key:generate`

## IMPORTANT!!!

_In your file .env you have to config:_

`DB_DATABASE=Haya`

`DB_USERNAME=root`

`DB_PASSWORD=YOUR_ROOT_PASS_IF_YOU_HAVE_PASSWORD`

#### Config tables in your database with migrate.

In the folder:

*back/*

execute:

`php artisan migrate`

#### Insert data in your database with the seed.

In the folder:

*back/*

execute:

`php artisan db:seed --class=HousesTableSeeder`

#### Now run db with artisan.

In the same folder (*back/*), execute:

`php artisan serve`

*Now server is working in localhost:8000.*

## Front, Vue.js cli.

The front was developed with the Framework *Vue.js* with *TypeScript* to have greater control of components, functions and variables.

Now install the dependencies...

In the folder:

*front/*

execute:

`npm i`

#### Now run the project.

execute:

`npm run serve`

*Now the project is working in localhost:8080.*

## Areas to improve.

I tried to config the cache with *php artisan cache*

Finally I decided to give the user the option *Cache::forever*

## Extra information.

*Libraries and webs that I have used to develop the front:*

##### Axios:

To *connect* with the api.

##### Fontawesome:

To use the *icons*.

##### Picsum:

To show random *photos*.

## About the Styles:
###### SCSS:

I used *BEM methodology* to config my styles.

The App is *100% responsive*: mobile, tablet and desktop.

***App created by Majdi Kokaly***